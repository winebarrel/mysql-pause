mysql-pause
===========

Description
-----------

mysql-pause is a proxy server to pause a query to MySQL.

Source Code
-----------

https://bitbucket.org/winebarrel/mysql-pause

Dependency
----------

* EventMachine
* RExec

Install
-------

gem install mysql-pause

Example
-------

Start server
^^^^^^^^^^^^
::

  shell> mysql-pause start
  Starting daemon...
  Waiting for daemon to start...
  Daemon status: running pid=56921

::

  shell> mysql -h 127.0.0.1 -P 13306 -u scott -ptiger
  mysql> select 1;
  +---+
  | 1 |
  +---+
  | 1 |
  +---+
  1 row in set (0.00 sec)

Pause
^^^^^
::

  shell> mpctl pause

::

  mysql> select 1;
  (...no response...)

Resume
^^^^^^
::

  shell> mpctl resume

::

  (...resume response...)
  +---+
  | 1 |
  +---+
  | 1 |
  +---+
  1 row in set (18.01 sec)

Extend MySQL adapter
--------------------

Install
^^^^^^^

gem install mysql-pause-adapter-ext

Example code
^^^^^^^^^^^^
::

  require 'active_record'
  require 'mysql-pause/adapter-ext'
  
  ActiveRecord::Base.establish_connection(
    :adapter   => 'mysql2',
    :host      => '127.0.0.1',
    :port      => 13306,
    :username  => 'scott',
    :password  => 'tiger',
    :database  => 'employees',
    :reconnect => true,
  )
  
  class Employee < ActiveRecord::Base; end
  
  loop do
    employee = Employee.new
    employee.first_name = 'Taro'
    employee.last_name = 'Yamada'
    employee.save!
    sleep 3
  end

Behavior
^^^^^^^^
::

  log> DEBUG -- :    (0.6ms)  BEGIN
  log> DEBUG -- :   SQL (0.7ms)  INSERT INTO `employees` (`first_name`, `last_name`) VALUES ('Taro', 'Yamada')
  log> DEBUG -- :    (0.8ms)  COMMIT
  log> DEBUG -- :    (0.6ms)  BEGIN

::

  shell> mpctl pause
  shell> service mysql restart
  shell> mpctl resume

::

  log> DEBUG -- :    (0.6ms)  BEGIN
  (...handle error and reconnect...)
  log> DEBUG -- : Mysql2::Error: %MYSQL_PAUSE%;;1000;;Aborted backend connection: BEGIN
  log> WARN -- : ActiveRecord::StatementInvalid: Mysql2::Error: %MYSQL_PAUSE%;;1000;;Aborted backend connection: BEGIN
  log> DEBUG -- :    (0.3ms)  BEGIN
  log> DEBUG -- :   SQL (2.1ms)  INSERT INTO `employees` (`first_name`, `last_name`) VALUES ('Taro', 'Yamada')
  log> DEBUG -- :    (0.8ms)  COMMIT
