$: << File.expand_path(File.dirname(__FILE__) + '/lib')
require 'mysql-pause/constants'

Gem::Specification.new do |spec|
  spec.name              = 'mysql-pause'
  spec.version           = Version
  spec.summary           = 'mysql-pause is a proxy server to pause a query to MySQL.'
  spec.require_paths     = %w(lib)
  spec.files             = %w(README) + Dir.glob('bin/**/*') + Dir.glob('lib/**/*')
  spec.author            = 'winebarrel'
  spec.email             = 'sgwr_dts@yahoo.co.jp'
  spec.homepage          = 'https://bitbucket.org/winebarrel/mysql-pause'
  spec.bindir            = 'bin'
  spec.executables << 'mysql-pause'
  spec.executables << 'mpctl'
  spec.add_dependency('eventmachine', '~> 1.0.3')
  spec.add_dependency('rexec', '~> 1.5.1')
  spec.add_dependency('mysql2')
end
