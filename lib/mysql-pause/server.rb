require 'drb/drb'
require 'fileutils'
require 'eventmachine'
require 'rexec'
require 'rexec/daemon'

require 'mysql-pause/proxy'

module RExec; module Daemon; class Base
  def self.daemon_name; APP_NAME; end
end; end; end

module MysqlPause
  class Server < RExec::Daemon::Base

    class << self
      def options=(options)
        @@options = options
        @@base_directory = options[:working_dir]
      end

      def run
        @@control_options = {
          :pause        => false,
          :interval     => @@options[:interval],
          :debug        => @@options[:debug],
          :ping_user    => @@options[:ping_user],
          :ping_pass    => @@options[:ping_pass],
        }

        # start DRb
        FileUtils.rm_f(@@options[:socket])
        DRb.start_service("drbunix:#{@@options[:socket]}", @@control_options)
        File.chmod(0700, @@options[:socket])
        at_exit { FileUtils.rm_f(@@options[:socket]) }

        EM.epoll
        EM.threadpool_size = @@options[:threads] if @@options[:threads]

        EM.run {
          EM.start_server(
            @@options[:addr], @@options[:port], MysqlPause::Proxy,
            @@options[:backend_addr], @@options[:backend_port], @@control_options)
        }
      end
    end # self

  end # Server
end # MysqlPause
