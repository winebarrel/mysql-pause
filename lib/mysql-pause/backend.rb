require 'eventmachine'

module MysqlPause
  class Backend < EM::Connection
    def initialize(proxy)
      @proxy = proxy
      @logger = proxy.logger
    end

    def post_init
      if (peername = get_peername)
        @connect_from = Socket.unpack_sockaddr_in(peername)
      end

      if @proxy and @proxy.options[:debug]
        port, ip = @connect_from
        @logger.debug("backend: connect from #{ip}:#{port}")
      end
    end

    def unbind
      if @proxy and @proxy.options[:debug]
        port, ip = @connect_from
        @logger.debug("backend: unbind connection from #{ip}:#{port}")
      end
    end

    def receive_data(data)
      if not @proxy or @proxy.error?
        @logger.info("backend: proxy connection error: #{data.inspect}")
        close_proxy_connection
        close_connection_after_writing
      else
        @proxy.send_data(data)
      end
    end

    private

    def close_proxy_connection
      @proxy.close_connection if @proxy
    rescue Exception => e
      @logger.warn("#{e.class.name}: #{e.message}")
    end
  end # Backend
end # MysqlPause
