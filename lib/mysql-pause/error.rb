module MysqlPause
  class Error

    ERROR_HEADER = '%MYSQL_PAUSE%'
    ERROR_SEPARATOR = ';;'
    ERROR_LIST = {}

    class << self
      def create_error_message(error_code, sequence_id)
        # fetch error info
        mysql_error_code, sql_state, message = ERROR_LIST[error_code]

        # create mysql error message
        message = [ERROR_HEADER, error_code, message].join(ERROR_SEPARATOR)

        # create header
        mysql_error_code = convert_to_chars(mysql_error_code, 2)
        payload = ["\xFF", mysql_error_code, '#', sql_state, message].join
        payload_length = convert_to_chars(payload.length, 3)
        sequence_id = convert_to_chars(sequence_id, 1)

        [payload_length, sequence_id, payload].join
      end

      def mysql_pause_error?(e)
        /#{Regexp.escape(ERROR_HEADER)}/ =~ e.message
      end

      private

      def convert_to_chars(number, length)
        (0...length).map {|i| (number >> (8 * i)) & 0xFF }.pack("C*")
      end

      def define_error(name, code, values)
        const_set(name, code)
        ERROR_LIST[code] = values
      end
    end # self

    # error list
    define_error :ABORTED_BACKEND_CONNECTION, 1000, [2013, '08S01', 'Aborted backend connection']

  end # Errot
end # MysqlPause
