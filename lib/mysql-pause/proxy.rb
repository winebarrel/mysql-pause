require 'eventmachine'
require 'mysql-pause/backend'
require 'mysql-pause/error'
require 'logger'
require 'socket'
require 'mysql2/em'

module MysqlPause
  RETRY_LIMIT = 100
  RETRY_INTERVAL = 3

  class Proxy < EM::Connection
    attr_reader :options
    attr_reader :logger

    def initialize(be_host, be_port, options)
      @options = options
      @logger = Logger.new($stdout)

      n = 0

      begin
        ping(be_host, be_port)
        @backend = EM.connect(be_host, be_port, MysqlPause::Backend, self)
        raise('connection error') if @backend.error?
      rescue => e
        @logger.error("#{e.class.name}: #{e.message}")

        if n < RETRY_LIMIT
          close_backend_connection
          sleep RETRY_INTERVAL
          @logger.warn("connection retry")
          n += 1
          retry
        else
          @logger.error("connection abort")
          raise e
        end
      end
    end

    def post_init
      if (peername = get_peername)
        @connect_from = Socket.unpack_sockaddr_in(peername)
      end

      if @options[:debug]
        port, ip = @connect_from
        @logger.debug("proxy: connect from #{ip}:#{port}")
      end
    end

    def unbind
      close_backend_connection

      if @options[:debug]
        port, ip = @connect_from
        @logger.debug("proxy: unbind connection from #{ip}:#{port}")
      end
    end

    def receive_data(data)
      EM.defer {
        if @options[:pause]
          @logger.info("pause: #{data.inspect}")
          sleep(@options[:interval]) while @options[:pause]
          @logger.info("resume: #{data.inspect}")
        end

        if @backend.error?
          @logger.info("proxy: backend connection error: #{data.inspect}")
          close_backend_connection

          payload_length, sequence_id = parse_mysql_packet(data)
          error_message = MysqlPause::Error.create_error_message(MysqlPause::Error::ABORTED_BACKEND_CONNECTION, sequence_id + 1)
          send_data(error_message)

          close_connection_after_writing
        else
          @backend.send_data(data)
        end
      }
    end

    private

    def parse_mysql_packet(data)
      header = data.slice(0, 4).unpack("C4")
      payload_length = (0..2).map {|i| header[i] << (8 * i) }.inject(0) {|r, i| r + i }
      sequence_id = header[3]
      [payload_length, sequence_id]
    end

    def close_backend_connection
      @backend.close_connection if @backend
    rescue Exception => e
      @logger.warn("#{e.class.name}: #{e.message}")
    end

    def ping(addr, port)
      mysqld_is_alive = false
      client = nil

      begin
        client = Mysql2::EM::Client.new(:host => addr, :port => port, :username => @options[:ping_user], :password => @options[:ping_pass])
        mysqld_is_alive = client.ping
      rescue Exception => e
        @logger.warn("#{e.class.name}: #{e.message}")
        mysqld_is_alive = false
      ensure
        begin
          client.close if client
        rescue Exception => e
          @logger.warn("#{e.class.name}: #{e.message}")
        end
      end

      return mysqld_is_alive
    end

    def connect_from
      return @connect_from if @connect_from

      peername = get_peername
      return nil unless peername

      @connect_from = Socket.unpack_sockaddr_in(peername)
    end
  end # Proxy
end # MysqlPause
