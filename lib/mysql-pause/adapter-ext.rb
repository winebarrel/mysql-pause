require 'active_record'
require 'active_record/connection_adapters/abstract_mysql_adapter'
require 'mysql-pause/error'

module ActiveRecord
  module ConnectionAdapters
    class AbstractMysqlAdapter

      alias mysql_pause_execute_orig execute

      def execute(sql, name = nil)
        begin
          mysql_pause_execute_orig(sql, name)
        rescue => e
          raise(e) unless MysqlPause::Error.mysql_pause_error?(e)

          if @logger
            @logger.warn("handle mysql-pause error and reconnect: #{e.class.name}: #{e.message}")
          end

          reconnect!
          retry
        end
      end

    end # AbstractMysqlAdapter
  end # ConnectionAdapters
end # ActiveRecord
