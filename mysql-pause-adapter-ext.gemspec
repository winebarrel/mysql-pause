$: << File.expand_path(File.dirname(__FILE__) + '/lib')
require 'mysql-pause/constants'

Gem::Specification.new do |spec|
  spec.name              = 'mysql-pause-adapter-ext'
  spec.version           = Version
  spec.summary           = 'mysql adapter extension for mysql-pause'
  spec.require_paths     = %w(lib)
  spec.files             = %w(README) + Dir.glob('lib/mysql-pause/{adapter-ext,error}.rb')
  spec.author            = 'winebarrel'
  spec.email             = 'sgwr_dts@yahoo.co.jp'
  spec.homepage          = 'https://bitbucket.org/winebarrel/mysql-pause'
end
