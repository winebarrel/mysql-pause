#!/usr/bin/env ruby
$: << File.expand_path(File.dirname(__FILE__) + '/../lib')

require 'optparse'
require 'mysql-pause/constants'
require 'mysql-pause/server'

options = nil

ARGV.options do |parser|
  daemon_commands = %w(start stop restart status)

  options = {
    :addr => '0.0.0.0',
    :port => 13306,
    :backend_addr => '127.0.0.1',
    :backend_port => 3306,
    :interval => 3,
    :working_dir => '/var',
    :socket => "/var/tmp/#{APP_NAME}.sock",
    :debug => false,
  }

  parser.on('-a', '--addr=ADDR'        ) {|v| options[:addr]         = v      }
  parser.on('-p', '--port=PORT'        ) {|v| options[:port]         = v.to_i }
  parser.on('-A', '--backend-addr=ADDR') {|v| options[:backend_addr] = v      }
  parser.on('-P', '--backend-port=PORT') {|v| options[:backend_port] = v.to_i }
  parser.on('-i', '--inerval=N'        ) {|v| options[:interval]     = v.to_i }
  parser.on('-n', '--threads=SIZE'     ) {|v| options[:threads]      = v.to_i }
  parser.on('-W', '--working-dir=DIR'  ) {|v| options[:working_dir]  = v      }
  parser.on('-S', '--socket=SOCK_FILE' ) {|v| options[:socket]       = v      }
  parser.on('',   '--ping-user=USER'   ) {|v| options[:ping_user]    = v      }
  parser.on('',   '--ping-pass=PASS'   ) {|v| options[:ping_pass]    = v      }
  parser.on('',   '--debug'            ) {    options[:debug]        = true   }

  help_and_exit = lambda do |v|
    $stderr.puts parser.help.sub(APP_NAME, "#{APP_NAME} {#{daemon_commands.join('|')}}")
    exit 1
  end

  parser.on('-h', '--help', &help_and_exit)

  parser.parse!

  unless ARGV.length == 1 and daemon_commands.include?(ARGV[0])
    help_and_exit.call(true)
  end
end # parse options

MysqlPause::Server.options = options
MysqlPause::Server.daemonize
