#!/usr/bin/env ruby
$: << File.expand_path(File.dirname(__FILE__) + '/../lib')

require 'drb/drb'
require 'optparse'
require 'mysql-pause/constants'
require 'yaml'

COMMAND_NAME = File.basename(__FILE__)

COMMANDS = {
  'pause' => lambda {|server_options|
    server_options[:pause] = true
  },
  'resume' => lambda {|server_options|
    server_options[:pause] = false
  },
  'status' => lambda {|server_options|
    h = {}
    server_options.to_hash.each {|k, v| h[k.to_s] = v }
    puts YAML.dump(h)
  },
  'set-interval' => lambda {|server_options, v|
    server_options[:interval] = v.to_i
  },
  'set-debug' => lambda {|server_options, v|
    server_options[:debug] = !!(/#{Regexp.escape(v)}/i =~ 'true')
  },
}

options = nil

ARGV.options do |parser|
  cmds = COMMANDS.keys

  options = {
    :socket => "/var/tmp/#{APP_NAME}.sock",
  }

  parser.on('-S', '--socket=SOCK_FILE') {|v| options[:socket] = v }

  help_and_exit = lambda do |v|
    $stderr.puts parser.help.sub(COMMAND_NAME, "#{COMMAND_NAME} {#{cmds.join('|')}}")
    exit 1
  end

  parser.on('-h', '--help', &help_and_exit)

  parser.parse!

  unless ARGV.length >= 1 and cmds.include?(ARGV[0])
    help_and_exit.call(true)
  end
end # parse options

server_options = DRbObject.new_with_uri("drbunix:#{options[:socket]}")

cmd = COMMANDS.fetch(ARGV[0])

if cmd.arity > 1
  cmd.call(server_options, ARGV[1])
else
  cmd.call(server_options)
end
